﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON
{
    public class Empleado
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Matricula { get; set; }
        public override string ToString()
        {
            return string.Format("{0} ({1}) {2}", Nombre, Apellidos, Matricula);
        }
    }
}
